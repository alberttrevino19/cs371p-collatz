var searchData=
[
  ['check_5fmax',['check_max',['../Collatz_8cpp.html#a5ee3840f01c83c5b1f59a679fdd9e838',1,'check_max(int i, int max_cycle):&#160;Collatz.cpp'],['../Collatz_8hpp.html#a5ee3840f01c83c5b1f59a679fdd9e838',1,'check_max(int i, int max_cycle):&#160;Collatz.cpp']]],
  ['collatz_5feval',['collatz_eval',['../Collatz_8cpp.html#ab5fc1300db8c0fa3680bafb19b838fe3',1,'collatz_eval(const pair&lt; int, int &gt; &amp;):&#160;Collatz.cpp'],['../Collatz_8hpp.html#a52d2fa1b09ed6ed338cb7931ae376986',1,'collatz_eval(const pair&lt; int, int &gt; &amp;):&#160;Collatz.cpp']]],
  ['collatz_5fprint',['collatz_print',['../Collatz_8cpp.html#abc6faa0ba3de27b9eb9b762dedf41f83',1,'collatz_print(ostream &amp;, const tuple&lt; int, int, int &gt; &amp;):&#160;Collatz.cpp'],['../Collatz_8hpp.html#abc6faa0ba3de27b9eb9b762dedf41f83',1,'collatz_print(ostream &amp;, const tuple&lt; int, int, int &gt; &amp;):&#160;Collatz.cpp']]],
  ['collatz_5fread',['collatz_read',['../Collatz_8cpp.html#a56b74aeeda96af4fa1cac147e62dc7e5',1,'collatz_read(const string &amp;):&#160;Collatz.cpp'],['../Collatz_8hpp.html#a030060205cce5a0fef7c6bef2d915c9f',1,'collatz_read(const string &amp;):&#160;Collatz.cpp']]],
  ['collatz_5fsolve',['collatz_solve',['../Collatz_8cpp.html#ac8d784fbd05e60bbd57a4b2db5ba6b87',1,'collatz_solve(istream &amp;, ostream &amp;):&#160;Collatz.cpp'],['../Collatz_8hpp.html#ac8d784fbd05e60bbd57a4b2db5ba6b87',1,'collatz_solve(istream &amp;, ostream &amp;):&#160;Collatz.cpp']]],
  ['cycle_5flength',['cycle_length',['../Collatz_8cpp.html#a7adcb84967b275f654ba4b42c0f8a2ad',1,'cycle_length(int i, int j):&#160;Collatz.cpp'],['../Collatz_8hpp.html#a7adcb84967b275f654ba4b42c0f8a2ad',1,'cycle_length(int i, int j):&#160;Collatz.cpp']]]
];
