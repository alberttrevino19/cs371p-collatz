// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

// -------------
// cycle_length
// -------------
TEST(CollatzFixture, length0) {
    ASSERT_EQ(cycle_length(1, 10), 20);
}

TEST(CollatzFixture, length1) {
    ASSERT_EQ(cycle_length(1, 2000), 182);
}

// -------------
// check_max
// -------------
TEST(CollatzFixture, max1) {
    ASSERT_EQ(check_max(1, 2000), 2000);
}

TEST(CollatzFixture, max2) {
    ASSERT_EQ(check_max(246, 246), 246);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(1000, 1000)), make_tuple(1000, 1000, 112));
}

TEST(CollatzFixture, eval5) {
    ASSERT_EQ(collatz_eval(make_pair(1, 1)), make_tuple(1, 1, 1));
}

TEST(CollatzFixture, eval6) {
    ASSERT_EQ(collatz_eval(make_pair(200, 100)), make_tuple(200, 100, 125));
}

TEST(CollatzFixture, eval7) {
    ASSERT_EQ(collatz_eval(make_pair(837799, 837799)), make_tuple(837799, 837799, 525));
}

TEST(CollatzFixture, eval8) {
    ASSERT_EQ(collatz_eval(make_pair(10, 100)), make_tuple(10, 100, 119));
}

TEST(CollatzFixture, eval9) {
    ASSERT_EQ(collatz_eval(make_pair(1, 2000)), make_tuple(1, 2000, 182));
}

TEST(CollatzFixture, eval10) {
    ASSERT_EQ(collatz_eval(make_pair(11000, 12000)), make_tuple(11000, 12000, 250));
}

TEST(CollatzFixture, eval11) {
    ASSERT_EQ(collatz_eval(make_pair(1000, 2000)), make_tuple(1000, 2000, 182));
}

TEST(CollatzFixture, eval12) {
    ASSERT_EQ(collatz_eval(make_pair(1, 999999)), make_tuple(1, 999999, 525));
}

TEST(CollatzFixture, eval13) {
    ASSERT_EQ(collatz_eval(make_pair(671121, 785994)), make_tuple(671121, 785994, 504));
}
// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}
